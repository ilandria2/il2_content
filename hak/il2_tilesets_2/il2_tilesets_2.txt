[Title]
Diademus Underdark Spidercaves 3.5 Tileset with Minimaps by Lord of Worms (updated for NWN 1.69 by Cuprius)

[ULR]
http://nwvault.ign.com/View.php?view=Hakpaks.Detail&id=7536

[Description]
Updates to the original Diademus Underdark Spidercaves 3.5 with Minimaps by Lord of Worms are:
- doortypes.2da is NWN 1.69 version with Diademus' Drowdoor01 added in line 159 (this was in line 181 in the NWN 1.68 version but NWN 1.69 already uses that line
- usc01.set updated from Diademus' original version to reflect Drowdoor01 now on doortypes.2da line 159
- environment.2da is NWN 1.69 version with line for this hak added in line 26
- loadscreens.2da is NWN 1.69 version with loadscreens for this hak added in line 260-263
- areag.ini is the NWN 1.69 version with entries for this tileset added

Note: if you use this hak in a module that formerly used an NWN 1.68 version of Diademus' hak (dia_underdark025b.hak or dia_udrkspid3.hak), you must delete Diademus' unique custom cave door from all areas that use it (the door will look strange in the toolset) then readd any doors you delete.  You must also reselect loadscreens for all areas that use the Underdark Spidercaves tileset.  This is due to the .2da changes described above.